//IIFE

(function(){
  
  var RegApp = angular.module("RegApp", []);

  var RegCtrl = function() {
    var regCtrl = this;

    // no var in this line
    regCtrl.clearForm = function() {
      regCtrl.name = "";
      regCtrl.email = "";
      regCtrl.phone = "";
      regCtrl.comment = "";
      regCtrl.exp = "neutral";
    };

    regCtrl.submitForm = function() {
      for (var i in regCtrl) {
        if (typeof regCtrl[i] === "string") {
          console.log('The input of %s is %s', i, regCtrl[i]);
        }
      }
      regCtrl.clearForm();
    };

    // initialize form
    regCtrl.clearForm();

  };
  RegApp.controller("RegCtrl", RegCtrl);

})();


    /* Using chaining John Papa format 
    angular
      .module("RegApp", [])

      .controller("RegCtrl", function(){

      });
      */
